FROM node:8.11-alpine

WORKDIR /app

COPY ./src/package.json ./
COPY ./src/package-lock.json ./

RUN npm i
COPY ./src .
COPY ./init.sh ./v.txt /

#CMD ["node","index.js"]
ENTRYPOINT ["node","index.js"]
